package krasov.testproject.mvp.view;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import krasov.testproject.data.network.model.ResponseModel;

/**
 * Created by user on 07.11.2017.
 */

public interface MainView extends BaseView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void showLoading();

    @StateStrategyType(OneExecutionStateStrategy.class)
    void hideLoading();

    @StateStrategyType(SingleStateStrategy.class)
    void setupListView(List<ResponseModel> responseModelList);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void startFullView(ResponseModel responseModel);
}
