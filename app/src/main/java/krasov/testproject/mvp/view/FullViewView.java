package krasov.testproject.mvp.view;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import krasov.testproject.data.network.model.ResponseModel;

/**
 * Created by user on 08.11.2017.
 */

public interface FullViewView extends BaseView {

    @StateStrategyType(SingleStateStrategy.class)
    void initFields(ResponseModel responseModel);

}
