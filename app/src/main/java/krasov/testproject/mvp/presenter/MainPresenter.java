package krasov.testproject.mvp.presenter;

import android.content.res.Resources;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;
import krasov.testproject.App;
import krasov.testproject.R;
import krasov.testproject.data.network.model.ResponseModel;
import krasov.testproject.domain.MainInteractor;
import krasov.testproject.mvp.view.MainView;
import krasov.testproject.utils.CommonUtils;
import krasov.testproject.utils.Constants;

/**
 * Created by user on 07.11.2017.
 */

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {
    private static final String TAG = Constants.TAG_PREFIX + "MainPresenter";
    @Inject
    CommonUtils utils;
    @Inject
    MainInteractor mainInteractor;
    @Inject
    Resources resources;


    public MainPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if (utils.isNetworkConnected()) {
            addSubscription(
                    mainInteractor.getPosts()
                            .subscribeWith(new DisposableSingleObserver<List<ResponseModel>>() {
                                @Override
                                public void onSuccess(List<ResponseModel> responseModels) {
                                    getViewState().setupListView(responseModels);
                                    getViewState().hideLoading();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    getViewState().hideLoading();
                                    getViewState().showToast(resources.getString(R.string.error_load));
                                }

                                @Override
                                protected void onStart() {
                                    super.onStart();
                                    getViewState().showLoading();
                                }
                            })
            );
        } else {
            getViewState().showToast(resources.getString(R.string.network_disable));
        }
    }


    public void onItemClick(ResponseModel responseModel) {
        getViewState().startFullView(responseModel);
    }
}
