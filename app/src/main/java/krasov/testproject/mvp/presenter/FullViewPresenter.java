package krasov.testproject.mvp.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import krasov.testproject.data.network.model.ResponseModel;
import krasov.testproject.mvp.view.FullViewView;
import krasov.testproject.utils.Constants;

/**
 * Created by user on 08.11.2017.
 */

@InjectViewState
public class FullViewPresenter extends MvpPresenter<FullViewView> {
    private static final String TAG = Constants.TAG_PREFIX + "FullViewPresenter";
    private ResponseModel responseModel;

    public FullViewPresenter(ResponseModel responseModel) {
        this.responseModel = responseModel;
    }

    @Override
    public void attachView(FullViewView view) {
        super.attachView(view);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().initFields(responseModel);
    }
}
