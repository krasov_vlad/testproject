package krasov.testproject.mvp.presenter;

import com.arellomobile.mvp.MvpPresenter;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import krasov.testproject.mvp.view.BaseView;

/**
 * Created by user on 07.11.2017.
 */

public class BasePresenter<View extends BaseView> extends MvpPresenter<View> {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void addSubscription(@NonNull Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
