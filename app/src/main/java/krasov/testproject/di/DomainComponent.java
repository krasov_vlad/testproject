package krasov.testproject.di;

import dagger.Subcomponent;
import krasov.testproject.di.modules.RetrofitModule;
import krasov.testproject.di.scopes.DomainScope;
import krasov.testproject.domain.MainInteractor;

/**
 * Created by user on 08.11.2017.
 */

@Subcomponent(modules = {RetrofitModule.class})
@DomainScope
public interface DomainComponent {
    void inject(MainInteractor mainInteractor);
}
