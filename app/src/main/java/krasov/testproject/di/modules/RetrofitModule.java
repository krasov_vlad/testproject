package krasov.testproject.di.modules;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import krasov.testproject.data.network.PostService;
import krasov.testproject.data.network.PostsApi;
import krasov.testproject.di.scopes.DomainScope;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 08.11.2017.
 */
@Module
public class RetrofitModule {

    @Provides
    @DomainScope
    Retrofit provideRetrofit(Retrofit.Builder builder) {
        return builder.baseUrl("https://jsonplaceholder.typicode.com/").build();
    }

    @Provides
    @DomainScope
    Retrofit.Builder provideBuilder(Converter.Factory factory) {
        return new Retrofit.Builder()
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @Provides
    @DomainScope
    Converter.Factory provideFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @DomainScope
    PostsApi provideApi(Retrofit retrofit) {
        return retrofit.create(PostsApi.class);
    }

    @Provides
    @DomainScope
    PostService providePostService(PostsApi postsApi) {
        return new PostService(postsApi);
    }
}
