package krasov.testproject.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by user on 08.11.2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainScope {
}

