package krasov.testproject.di;

import javax.inject.Singleton;

import dagger.Component;
import krasov.testproject.di.modules.AppModule;
import krasov.testproject.di.modules.RetrofitModule;
import krasov.testproject.domain.MainInteractor;
import krasov.testproject.mvp.presenter.MainPresenter;

/**
 * Created by user on 07.11.2017.
 */


@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    void inject(MainPresenter mainPresenter);

    DomainComponent plusDomainComponent(RetrofitModule retrofitModule);
}
