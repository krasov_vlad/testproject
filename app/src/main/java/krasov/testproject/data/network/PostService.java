package krasov.testproject.data.network;

import java.util.List;

import io.reactivex.Single;
import krasov.testproject.data.network.model.ResponseModel;

/**
 * Created by user on 08.11.2017.
 */

public class PostService {
    private PostsApi postsApi;

    public PostService(PostsApi postsApi) {
        this.postsApi = postsApi;
    }

    public Single<List<ResponseModel>> getPosts() {
        return postsApi.getPosts();
    }
}
