package krasov.testproject.data.network;

import java.util.List;

import io.reactivex.Single;
import krasov.testproject.data.network.model.ResponseModel;
import retrofit2.http.GET;

/**
 * Created by user on 08.11.2017.
 */

public interface PostsApi {
    @GET("/posts")
    Single<List<ResponseModel>> getPosts();
}
