package krasov.testproject.ui.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;
import krasov.testproject.R;
import krasov.testproject.data.network.model.ResponseModel;

/**
 * Created by user on 08.11.2017.
 */

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PLViewHolder> {
    private List<ResponseModel> responseModels;
    private PublishSubject<ResponseModel> publishSubject = PublishSubject.create();

    public PostListAdapter(List<ResponseModel> responseModels) {
        this.responseModels = responseModels;
    }

    public PublishSubject<ResponseModel> getPublishSubject() {
        return publishSubject;
    }

    @Override
    public PLViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent, false);
        return new PLViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PLViewHolder holder, int position) {
        holder.textView.setText(responseModels.get(position).getTitle());
        holder.cardView.setOnClickListener(v -> publishSubject.onNext(responseModels.get(position)));
    }

    @Override
    public int getItemCount() {
        return responseModels.size();
    }

    public class PLViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text1)
        TextView textView;
        @BindView(R.id.cardView)
        CardView cardView;

        public PLViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
