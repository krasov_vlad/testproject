package krasov.testproject.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.testproject.R;
import krasov.testproject.data.network.model.ResponseModel;
import krasov.testproject.mvp.presenter.MainPresenter;
import krasov.testproject.mvp.view.MainView;
import krasov.testproject.ui.adapters.PostListAdapter;
import krasov.testproject.utils.Constants;

public class MainActivity extends BaseActivity implements MainView {
    private static final String TAG = Constants.TAG_PREFIX + "MainActivity";
    @InjectPresenter
    MainPresenter mainPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setupListView(List<ResponseModel> responseModelList) {
        PostListAdapter adapter = new PostListAdapter(responseModelList);
        recyclerView.setAdapter(adapter);
        adapter.getPublishSubject().subscribe(responseModel -> mainPresenter.onItemClick(responseModel));
    }

    @Override
    public void startFullView(ResponseModel responseModel) {
        Intent i = new Intent(this, FullViewActivity.class);
        i.putExtra(Constants.FULLVIEWNAME, responseModel);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
