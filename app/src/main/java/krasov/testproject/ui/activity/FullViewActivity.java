package krasov.testproject.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.testproject.R;
import krasov.testproject.data.network.model.ResponseModel;
import krasov.testproject.mvp.presenter.FullViewPresenter;
import krasov.testproject.mvp.view.FullViewView;
import krasov.testproject.utils.Constants;

public class FullViewActivity extends BaseActivity implements FullViewView {

    @InjectPresenter
    FullViewPresenter fullViewPresenter;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.body)
    TextView body;

    @ProvidePresenter
    FullViewPresenter fullViewPresenter() {
        return new FullViewPresenter(getIntent().getParcelableExtra(Constants.FULLVIEWNAME));
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_view);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void initFields(ResponseModel responseModel) {
        title.setText(responseModel.getTitle());
        body.setText(responseModel.getBody());
    }
}
