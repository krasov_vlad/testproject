package krasov.testproject;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import krasov.testproject.di.AppComponent;
import krasov.testproject.di.DaggerAppComponent;
import krasov.testproject.di.DomainComponent;
import krasov.testproject.di.modules.AppModule;
import krasov.testproject.di.modules.RetrofitModule;

/**
 * Created by user on 07.11.2017.
 */

public class App extends Application {
    private static AppComponent appComponent;
    private static DomainComponent domainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildAppComponent();
        domainComponent = buildDomainComponent();

//        if (LeakCanary.isInAnalyzerProcess(this)) {
//             This process is dedicated to LeakCanary for heap analysis.
//             You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);

    }

    public static DomainComponent buildDomainComponent() {
        domainComponent = appComponent.plusDomainComponent(new RetrofitModule());
        return domainComponent;
    }


    private AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static DomainComponent getDomainComponent() {
        return domainComponent;
    }
}
