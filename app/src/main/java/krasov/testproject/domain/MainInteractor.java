package krasov.testproject.domain;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import krasov.testproject.App;
import krasov.testproject.data.network.PostService;
import krasov.testproject.data.network.model.ResponseModel;
import krasov.testproject.di.modules.RetrofitModule;

/**
 * Created by user on 08.11.2017.
 */

public class MainInteractor {
    @Inject
    PostService postService;

    public MainInteractor() {
        App.getDomainComponent().inject(this);
    }


    public Single<List<ResponseModel>> getPosts() {
        return postService.getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
